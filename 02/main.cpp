#include <thread>
#include "prc.h"

int main (int argc, char**argv)
{
	PRC::prc_run = false;
	// PageRank calculator
	std::thread prc_thread(PRC::prc);

	// UI
	std::thread UI_thread(PRC::UI);

	// prc end
	prc_thread.join();

	// UI end
	UI_thread.join();

	return 0;
}