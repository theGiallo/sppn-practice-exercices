#pragma once

#include <string>
#include <memory>
#include <map>
#include <mutex>
// sonia
namespace PRC
{
	struct User
	{
		int id;
		std::string name;
		std::map<int, std::shared_ptr<User>> following;
	};

	using User = struct User;

	extern std::multimap<std::shared_ptr<User>,std::shared_ptr<User>> graph;
	
	extern std::map<std::string, std::shared_ptr<User>> users_by_name;
	extern std::map<int, std::shared_ptr<User>> users_by_id;

	extern std::string file_path;
	extern std::mutex data_mutex;

	bool loadGML(const std::string& file_path);
	bool savePR(const std::string& file_path);
	void clearData();

	// thread functions
	void prc();
	void UI();

	//debug
	void printGraph();

	extern bool prc_run,
	            prc_paused;
}