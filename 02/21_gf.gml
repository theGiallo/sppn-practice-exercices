graph
[
	node
	[
		id 0
		label "Andyroid1978"
	]
	node
	[
		id 1
		label "Anfex"
	]
	node
	[
		id 2
		label "Chosko"
	]
	node
	[
		id 3
		label "King_DuckZ"
	]
	node
	[
		id 4
		label "TagoWill"
	]
	node
	[
		id 5
		label "bznein"
	]
	node
	[
		id 6
		label "ebianchi"
	]
	node
	[
		id 7
		label "encelo"
	]
	node
	[
		id 8
		label "genep_ultron"
	]
	node
	[
		id 9
		label "gmrepo"
	]
	node
	[
		id 10
		label "lovlesh"
	]
	node
	[
		id 11
		label "paolinux86"
	]
	node
	[
		id 12
		label "plaoo"
	]
	node
	[
		id 13
		label "psicomante"
	]
	node
	[
		id 14
		label "sNada"
	]
	node
	[
		id 15
		label "sannysmoke"
	]
	node
	[
		id 16
		label "shenzi"
	]
	node
	[
		id 17
		label "sterro88"
	]
	node
	[
		id 18
		label "theGiallo"
	]
	node
	[
		id 19
		label "viniciusmarcius"
	]
	node
	[
		id 20
		label "vivaladav"
	]
	edge
	[
		id 0
		source 1
		target 7
		directed 1
	]
	edge
	[
		id 1
		source 2
		target 13
		directed 1
	]
	edge
	[
		id 2
		source 2
		target 17
		directed 1
	]
	edge
	[
		id 3
		source 2
		target 14
		directed 1
	]
	edge
	[
		id 4
		source 3
		target 18
		directed 1
	]
	edge
	[
		id 5
		source 4
		target 16
		directed 1
	]
	edge
	[
		id 6
		source 7
		target 18
		directed 1
	]
	edge
	[
		id 7
		source 7
		target 9
		directed 1
	]
	edge
	[
		id 8
		source 7
		target 1
		directed 1
	]
	edge
	[
		id 9
		source 7
		target 20
		directed 1
	]
	edge
	[
		id 10
		source 9
		target 18
		directed 1
	]
	edge
	[
		id 11
		source 9
		target 7
		directed 1
	]
	edge
	[
		id 12
		source 9
		target 13
		directed 1
	]
	edge
	[
		id 13
		source 11
		target 12
		directed 1
	]
	edge
	[
		id 14
		source 12
		target 18
		directed 1
	]
	edge
	[
		id 15
		source 12
		target 11
		directed 1
	]
	edge
	[
		id 16
		source 13
		target 18
		directed 1
	]
	edge
	[
		id 17
		source 13
		target 9
		directed 1
	]
	edge
	[
		id 18
		source 13
		target 7
		directed 1
	]
	edge
	[
		id 19
		source 14
		target 17
		directed 1
	]
	edge
	[
		id 20
		source 14
		target 2
		directed 1
	]
	edge
	[
		id 21
		source 14
		target 15
		directed 1
	]
	edge
	[
		id 22
		source 15
		target 14
		directed 1
	]
	edge
	[
		id 23
		source 16
		target 4
		directed 1
	]
	edge
	[
		id 24
		source 17
		target 13
		directed 1
	]
	edge
	[
		id 25
		source 17
		target 2
		directed 1
	]
	edge
	[
		id 26
		source 17
		target 14
		directed 1
	]
	edge
	[
		id 27
		source 18
		target 9
		directed 1
	]
	edge
	[
		id 28
		source 18
		target 3
		directed 1
	]
	edge
	[
		id 29
		source 18
		target 7
		directed 1
	]
	edge
	[
		id 30
		source 20
		target 7
		directed 1
	]
]