#include "prc.h"

#include <thread>
#include <unistd.h> // sleep
#include <iostream>
#include <fstream>      // std::ifstream

#define SWITCH_COMPLETE
#include <generic_switch.h>

// Variables
//---
std::multimap<std::shared_ptr<PRC::User>,std::shared_ptr<PRC::User>> PRC::graph;

std::map<std::string, std::shared_ptr<PRC::User>> PRC::users_by_name;
std::map<int, std::shared_ptr<PRC::User>> PRC::users_by_id;

std::string PRC::file_path;

std::mutex PRC::data_mutex;
bool PRC::prc_run = false;
bool PRC::prc_paused = true;


// Functions
//---

bool PRC::loadGML(const std::string& file_path)
{
	std::ifstream inf(file_path, std::ifstream::in);
	PRC::file_path = std::string(file_path);
	// std::string line;

	// while (std::getline(inf, line))
	// {
	// 	std::istringstream is_line(line);
	// 	std::string key;

	// 	if (std::getline(is_line, key, '['))
	// 	{

	// 	}
	// }

	char buf[100];
	
	inf>>std::ws>>buf;
	std::string graph_name(buf);

	inf>>std::ws>>buf;
	if (buf[0]!='[' || buf[1]!='\0')
	{
		std::cout<<"Expected a '[' instead found "<<std::endl<<buf<<"--end of found text--"<<std::endl;
		inf.close();
		return false;
	}

	// parse nodes and edges
	while(true)
	{
		inf>>std::ws>>buf;
		std::string what(buf);
		if (what == "node")
		// parse node
		{
			inf>>std::ws>>buf;
			if (buf[0]!='[' || buf[1]!='\0')
			{
				std::cout<<"Expected a '[' instead found"<<std::endl<<buf<<"--end of found text--"<<std::endl;
				inf.close();
				return false;
			}

			int id;
			std::string label;
			inf>>buf>>std::ws>>id>>std::ws
			>>buf>>std::ws;
			getline(inf, label, '"');
			getline(inf, label, '"');
			std::cout<<"label="<<label<<std::endl;
			
			inf>>std::ws>>buf;
			if (buf[0]!=']' || buf[1]!='\0')
			{
				std::cout<<"Expected a ']' instead found"<<std::endl<<buf<<"--end of found text--"<<std::endl;
				inf.close();
				return false;
			}

			std::shared_ptr<User> user (new User({id, label, std::map<int, std::shared_ptr<User>>()}));
			users_by_id.insert(std::pair<int, std::shared_ptr<PRC::User> >(id,user));
			users_by_name.insert(std::pair<std::string, std::shared_ptr<PRC::User> >(label,user));
		} else
		if (what == "edge")
		// parse edge
		{
			inf>>std::ws>>buf;
			if (buf[0]!='[' || buf[1]!='\0')
			{
				std::cout<<"Expected a '[' instead found "<<std::endl<<buf<<"--end of found text--"<<std::endl;
				inf.close();
				return false;
			}

			int id,
			    source,
			    target;
			inf>>buf>>std::ws>>id>>std::ws
			>>buf>>std::ws>>source
			>>buf>>std::ws>>target
			>>buf>>std::ws>>buf;

			inf>>std::ws>>buf;
			if (buf[0]!=']' || buf[1]!='\0')
			{
				std::cout<<"Expected a ']' instead found "<<std::endl<<buf<<"--end of found text--"<<std::endl;
				inf.close();
				return false;
			}

			std::shared_ptr<User> t = users_by_id[target];
			std::shared_ptr<User> s = users_by_id[source];
			s->following[t->id] = t;
			graph.insert(std::pair<std::shared_ptr<User>,std::shared_ptr<User>>(s,t));
		} else
		if (what == "]")
		// end of graph
		{
			break;
		} else
		{
			std::cout<<"wtf??? found "<<buf<<std::endl;
		}
	}

	inf.close();
	return true;
}
bool PRC::savePR(const std::string& file_path)
{

}
void PRC::clearData()
{
	data_mutex.lock();

	users_by_id.clear();
	users_by_name.clear();
	graph.clear();

	data_mutex.unlock();
}

// Thread functions
//---

void PRC::UI()
{
	std::string a_string;

	while (true)
	{
		std::getline (std::cin,a_string);

		SWITCH (a_string)
		{
		CASE ({"users count"})
			std::cout<<users_by_id.size()<<std::endl;
			break;
		CASE ({"start"})
			if (prc_run)
			{
				std::cout<<"already running!"<<std::endl;
			}
			prc_run = true;
			while(prc_paused){sleep(1);std::this_thread::yield();}
			std::cout<<"running"<<std::endl;
			break;
		CASE ({"pause"})
			if (!prc_run)
			{
				std::cout<<"not running!"<<std::endl;
			} else
			{
				prc_run = false;
				while(!prc_paused){std::this_thread::yield();}
				std::cout<<"paused"<<std::endl;
			}
			break;
		CASE ({"save as"})
			std::cout<<"where I have to write? ";
			std::getline (std::cin,a_string);
			std::cout<<"saving... "; std::cout.flush();
			savePR(a_string);
			std::cout<<"done."<<std::endl; std::cout.flush();
			break;
		CASE ({"load"})
			std::cout<<"where I have to load from? ";
			std::getline (std::cin,a_string);
			std::cout<<"loading... "; std::cout.flush();
			loadGML(a_string);
			std::cout<<"done."<<std::endl; std::cout.flush();
			break;
		CASE ({"clear"})
			bool not_answered = true;
			while (not_answered)
			{
				std::cout<<"All data will be deleted. Do you want to proceed? [Yes, No] ";
				std::getline (std::cin,a_string);

				SWITCH (a_string)
				{
				CASE ({"Yes"})
					clearData();
					std::cout<<"All data have been deleted"<<std::endl;
					not_answered = false;
					break;
				CASE ({"No"})
					not_answered = false;
					break;
				DEFAULT
					std::cout<<"Please write exactly \"Yes\" or \"No\""<<std::endl;
					break;
				} SWITCHEND
			}
			break;
		CASE ({"help"})
			std::cout<<"Commands:"<<std::endl<<std::endl<<
			"    start            starts the PageRank calculation"<<std::endl<<
			"    pause            pauses the PageRank calculation"<<std::endl<<
			"    save as	      saves the data in a simple file"<<std::endl<<
			"    load             loads data from a GML file"<<std::endl<<
			"    clear            clears all the data"<<std::endl<<
			"    users count      prints the number of users in the data"<<std::endl<<std::endl;
			break;
		DEFAULT
			std::cout<<"unrecognised request"<<std::endl;
		} SWITCHEND
	}
}

void PRC::prc()
{
	while (!prc_run){prc_paused=true; std::this_thread::yield();}
	prc_paused=false;
	
	printGraph();

	// <do the calculations>

}
void PRC::printGraph()
{
	for (std::multimap<std::shared_ptr<User>,std::shared_ptr<User>>::iterator it=graph.begin(); it!=graph.end(); ++it)
    {
    	std::cout << (*it).first->name << " => " << (*it).second->name << " following: " << '\n';
    	for(auto it2 = (*it).second->following.cbegin(); it2 != (*it).second->following.cend(); ++it2)
		{
		    std::cout << "\t" << /*users_by_id[it2->first]->name << " -> " << */ it2->second->name << "\n";
		}
    }	
}