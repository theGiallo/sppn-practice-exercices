#ifndef _GENGRAPH_H_
#define _GENGRAPH_H_

#include <algorithm>
#include <set>
#include <map>

struct graph
{
	std::map<int,std::string> nodes;
	std::set<std::pair<int,int>> edges;

	bool addEdge(int n0, int n1)
	{
		if (nodes.find(n0)==nodes.end())
		{
			return false;
		}
		if (nodes.find(n1)==nodes.end())
		{
			return false;
		}
		if (n0==n1)
		{
			return false;
		}
		if (n0 > n1)
		{
			std::swap(n0,n1);
		}

		edges.insert(std::pair<int,int>(n0,n1));
		return true;
	}
	bool findEdge(int n0, int n1, std::pair<int,int>& out_e)
	{
		if (n0 > n1)
		{
			std::swap(n0,n1);
		}
		std::set<std::pair<int,int>>::iterator it;
		it = edges.find(std::pair<int,int>(n0,n1));
		if (it == edges.end())
		{
			return false;
		}
		out_e = *it;
		return true;
	}
	void removeEdge(int n0, int n1)
	{
		if (n0 > n1)
		{
			std::swap(n0,n1);
		}
		edges.erase(std::pair<int,int>(n0,n1));
	}
};

#endif
