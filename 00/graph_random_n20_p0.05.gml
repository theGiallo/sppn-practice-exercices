graph
[
	node
	[
		id 0
		label "0"
		graphics
		[
			x 20
			y 0
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 1
		label "1"
		graphics
		[
			x 19.0211
			y 6.18034
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 2
		label "2"
		graphics
		[
			x 16.1803
			y 11.7557
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 3
		label "3"
		graphics
		[
			x 11.7557
			y 16.1803
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 4
		label "4"
		graphics
		[
			x 6.18034
			y 19.0211
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 5
		label "5"
		graphics
		[
			x -8.74228e-07
			y 20
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 6
		label "6"
		graphics
		[
			x -6.18034
			y 19.0211
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 7
		label "7"
		graphics
		[
			x -11.7557
			y 16.1803
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 8
		label "8"
		graphics
		[
			x -16.1803
			y 11.7557
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 9
		label "9"
		graphics
		[
			x -19.0211
			y 6.18034
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 10
		label "10"
		graphics
		[
			x -20
			y -1.74846e-06
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 11
		label "11"
		graphics
		[
			x -19.0211
			y -6.18034
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 12
		label "12"
		graphics
		[
			x -16.1803
			y -11.7557
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 13
		label "13"
		graphics
		[
			x -11.7557
			y -16.1803
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 14
		label "14"
		graphics
		[
			x -6.18034
			y -19.0211
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 15
		label "15"
		graphics
		[
			x 2.38498e-07
			y -20
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 16
		label "16"
		graphics
		[
			x 6.18034
			y -19.0211
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 17
		label "17"
		graphics
		[
			x 11.7557
			y -16.1803
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 18
		label "18"
		graphics
		[
			x 16.1803
			y -11.7557
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 19
		label "19"
		graphics
		[
			x 19.0211
			y -6.18034
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	edge
	[
		id 20
		source 0
		target 1
		directed 0
	]
	edge
	[
		id 21
		source 0
		target 7
		directed 0
	]
	edge
	[
		id 22
		source 0
		target 14
		directed 0
	]
	edge
	[
		id 23
		source 0
		target 18
		directed 0
	]
	edge
	[
		id 24
		source 1
		target 16
		directed 0
	]
	edge
	[
		id 25
		source 4
		target 17
		directed 0
	]
	edge
	[
		id 26
		source 6
		target 15
		directed 0
	]
	edge
	[
		id 27
		source 7
		target 18
		directed 0
	]
	edge
	[
		id 28
		source 8
		target 9
		directed 0
	]
	edge
	[
		id 29
		source 12
		target 15
		directed 0
	]
]