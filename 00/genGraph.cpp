#define _USE_MATH_DEFINES
#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <random>
#include <cmath>

#include <getoptpp/getopt_pp.h>

#include "genGraph.h"

using namespace GetOpt;
using namespace std;

const set<string> allowed_types = {"random", "small_world"};
enum RETURN
{
	OK = 0x0,
	NO_TYPE = 0x1<<0,
	WRONG_TYPE = 0x1<<1,
	NO_N = 0x1<<2,
	NO_P = 0x1<<3,
	NO_C = 0x1<<4,
	FOPEN_ERROR = 0x1<<5,
	COUNT
};

int main (int argc, char** argv)
{
	GetOpt_pp ops(argc, argv);
    ops.exceptions(ios::eofbit);

    // random initialization
	default_random_engine p_generator;
	uniform_real_distribution<float> p_distribution(0.0f,1.0f);
	default_random_engine node_generator;

    // read graph type
	string type;
	try {
	ops>>Option('t',"type",type);
	} catch (OptionNotFoundEx e)
	{
		cout<<"You have to specify a type of graph (-t type). Allowed types are:"<<endl;
		for (string t: allowed_types)
		{
			cout<<"\t"<<t<<endl;
		}
		exit(NO_TYPE);
	}
	cout<<"Requested graph of type "<<type<<endl;
	if (allowed_types.find(type)==allowed_types.end())
	{
		cout<<"Type not recognised. Allowed types are:"<<endl;
		for (string t: allowed_types)
		{
			cout<<"\t"<<t<<endl;
		}
		exit(WRONG_TYPE);
	}

	// read output file path
	string outfilepath;
	try {
	ops>>Option('o',"output",outfilepath);
	cout<<"The graph will be saved to "<<outfilepath<<endl;
	} catch (OptionNotFoundEx e)
	{
		outfilepath = "";
	}

	// the graph
	struct graph g;

	// random graph
	if (type.compare("random")==0)
	{
		uint err = OK;
		int n;
		try {
		ops>>Option('n',n);
		} catch (OptionNotFoundEx e)
		{
			cout<<"You have to specify the number of nodes (-n N)."<<endl;
			err |= NO_N;
		}
		float p;
		try {
		ops>>Option('p',p);
		} catch (OptionNotFoundEx e)
		{
			cout<<"You have to specify the connection probability (-p P)."<<endl;
			err |= NO_P;
		}
		if (err)
		{
			exit(err);
		}
		cout<<"n = "<<n<<endl<<"p = "<<p<<endl;
		if (outfilepath.compare("")==0)
		{
			stringstream ss;
			ss << "graph_"<<type<<"_n"<<n<<"_p"<<p<<".gml";
			outfilepath = ss.str();
			cout<<"The graph will be saved to "<<outfilepath<<endl;
		}
		
		// create the graph
		stringstream ss;
		
		// insert all nodes
		for (int i=0; i<n ; i++)
		{
			ss.str("");
			ss << i;
			g.nodes.insert(std::pair<int,string>(i,ss.str()));
		}

		// for each possible edge create it with probability p
		for (int i=0; i<n; i++)
		{
			for (int j=i+1; j<n; j++)
			{
				float pv;
				pv = p_distribution(p_generator);
				
				if (pv < p)
				// connect
				{
					g.addEdge(i,j);
				}
			}
		}
	} // /random graph

	// small world graph
	if (type.compare("small_world")==0)
	{
		int err = OK;
		int n;
		try {
		ops>>Option('n',n);
		} catch (OptionNotFoundEx e)
		{
			cout<<"You have to specify the number of nodes (-n N)."<<endl;
			err |= NO_N;
		}
		int c;
		try {
		ops>>Option('c',c);
		} catch (OptionNotFoundEx e)
		{
			cout<<"You have to specify the number of connected neighbors (-c C)."<<endl;
			err |= NO_C;
		}
		float p;
		try {
		ops>>Option('p',p);
		} catch (OptionNotFoundEx e)
		{
			cout<<"You have to specify the connection change probability (-p P)."<<endl;
			err |= NO_P;
		}
		if (err)
		{
			exit(err);
		}
		cout<<"n = "<<n<<endl<<"c = "<<c<<endl<<"p = "<<p<<endl;
		if (outfilepath.compare("")==0)
		{
			stringstream ss;
			ss << "graph_"<<type<<"_n"<<n<<"_c"<<c<<"_p"<<p<<".gml";
			outfilepath = ss.str();
			cout<<"The graph will be saved to "<<outfilepath<<endl;
		}

		// create the graph

		stringstream ss;
		
		// insert all nodes
		for (int i=0; i<n ; i++)
		{
			ss.str("");
			ss << i;
			g.nodes.insert(std::pair<int,string>(i,ss.str()));
		}
		
		// create initial neighborhood connections
		for (int i=0; i<n ; i++)
		{
			for (int j=1; j<=(c+1)/2; j++)
			{
				g.addEdge(i,(i+j)%n);
			}
			for (int j=1; j<=(c)/2; j++)
			{
				g.addEdge(i,(i-j+n)%n);
			}
		}

		// randomly change connections

		for (int k=1; k<(c+1)/2 ; k++)
		{
			// move clockwise
			for (int i=0; i<n ; i++)
			{
				pair<int,int> e;
				int c_node;

				// choose the k-nearest neighbor in a clockwise sense
				for (int j=i+1, nn=0; nn!=k; nn = (g.findEdge(i,j%n,e)?nn+1:nn), j++);

				c_node = e.first == i ? e.second : e.first;

				float pv;
				pv = p_distribution(p_generator);
				if (pv < p)
				// rewire
				{
					// select all possible nodes to connect
					std::vector<int> possible_nodes;
					possible_nodes.reserve(n-c-1);
					for (int node=0; node<n; node++)
					{
						if (node!=i && node!=c_node && g.findEdge(i,node,e))
						{
							possible_nodes.push_back(node);
						}
					}

					// uniformly choose the node to connect
					uniform_int_distribution<int> node_distribution(0,possible_nodes.size());
					int n_node = node_distribution(node_generator);

					// remove the old edge
					g.removeEdge(i,c_node);
					// add the new edge
					g.addEdge(i,possible_nodes[n_node]);
				}
			}
		}
	} // /small world graph

	fstream fout;
	fout.open (outfilepath, std::fstream::out);
	if (!fout.is_open())
	{
		return FOPEN_ERROR;
	}

	float angle = 2.0f*M_PI/(float)g.nodes.size();
	float radius = g.nodes.size();
  	fout << "graph"<<endl<<"["<<endl;

	// print nodes
  	for (pair<int,string> node: g.nodes)
	{
		fout<<"	node"<<endl
			<<"	["<<endl
			<<"		id "<<node.first<<endl
			<<"		label \""<<node.second<<"\""<<endl
			<<"		graphics"<<endl
			<<"		["<<endl
			<<"			x "<<radius*cos(angle*node.first)<<endl
			<<"			y "<<radius*sin(angle*node.first)<<endl
			<<"			z 0.0"<<endl
			<<"			w 1.0"<<endl
			<<"			h 1.0"<<endl
			<<"			fill \"#999999\""<<endl
			<<"		]"<<endl
			<<"	]"<<endl;
	}

	// print edges
	int id=g.nodes.size();
  	for (pair<int,int> edge: g.edges)
	{
		fout<<"	edge"<<endl
			<<"	["<<endl
			<<"		id "<<id<<endl
			<<"		source "<<edge.first<<endl
			<<"		target "<<edge.second<<endl
			<<"		directed 0"<<endl
			<<"	]"<<endl;
		++id;
	}
  	fout << "]";

  	fout.close();
  	cout<<"Done."<<endl;
  	cout<<"Generated a graph with"<<endl
  	<<"	"<<g.nodes.size()<<" nodes"<<endl
  	<<"	"<<g.edges.size()<<" edges"<<endl;
	return OK;
}