graph
[
	node
	[
		id 0
		label "0"
		graphics
		[
			x 20
			y 0
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 1
		label "1"
		graphics
		[
			x 19.0211
			y 6.18034
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 2
		label "2"
		graphics
		[
			x 16.1803
			y 11.7557
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 3
		label "3"
		graphics
		[
			x 11.7557
			y 16.1803
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 4
		label "4"
		graphics
		[
			x 6.18034
			y 19.0211
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 5
		label "5"
		graphics
		[
			x -8.74228e-07
			y 20
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 6
		label "6"
		graphics
		[
			x -6.18034
			y 19.0211
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 7
		label "7"
		graphics
		[
			x -11.7557
			y 16.1803
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 8
		label "8"
		graphics
		[
			x -16.1803
			y 11.7557
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 9
		label "9"
		graphics
		[
			x -19.0211
			y 6.18034
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 10
		label "10"
		graphics
		[
			x -20
			y -1.74846e-06
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 11
		label "11"
		graphics
		[
			x -19.0211
			y -6.18034
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 12
		label "12"
		graphics
		[
			x -16.1803
			y -11.7557
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 13
		label "13"
		graphics
		[
			x -11.7557
			y -16.1803
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 14
		label "14"
		graphics
		[
			x -6.18034
			y -19.0211
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 15
		label "15"
		graphics
		[
			x 2.38498e-07
			y -20
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 16
		label "16"
		graphics
		[
			x 6.18034
			y -19.0211
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 17
		label "17"
		graphics
		[
			x 11.7557
			y -16.1803
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 18
		label "18"
		graphics
		[
			x 16.1803
			y -11.7557
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	node
	[
		id 19
		label "19"
		graphics
		[
			x 19.0211
			y -6.18034
			z 0.0
			w 1.0
			h 1.0
			fill "#999999"
		]
	]
	edge
	[
		id 20
		source 0
		target 2
		directed 0
	]
	edge
	[
		id 21
		source 0
		target 13
		directed 0
	]
	edge
	[
		id 22
		source 0
		target 18
		directed 0
	]
	edge
	[
		id 23
		source 0
		target 19
		directed 0
	]
	edge
	[
		id 24
		source 1
		target 2
		directed 0
	]
	edge
	[
		id 25
		source 1
		target 3
		directed 0
	]
	edge
	[
		id 26
		source 1
		target 19
		directed 0
	]
	edge
	[
		id 27
		source 2
		target 3
		directed 0
	]
	edge
	[
		id 28
		source 2
		target 4
		directed 0
	]
	edge
	[
		id 29
		source 3
		target 4
		directed 0
	]
	edge
	[
		id 30
		source 3
		target 5
		directed 0
	]
	edge
	[
		id 31
		source 4
		target 5
		directed 0
	]
	edge
	[
		id 32
		source 4
		target 6
		directed 0
	]
	edge
	[
		id 33
		source 5
		target 6
		directed 0
	]
	edge
	[
		id 34
		source 5
		target 7
		directed 0
	]
	edge
	[
		id 35
		source 6
		target 8
		directed 0
	]
	edge
	[
		id 36
		source 7
		target 8
		directed 0
	]
	edge
	[
		id 37
		source 7
		target 9
		directed 0
	]
	edge
	[
		id 38
		source 8
		target 9
		directed 0
	]
	edge
	[
		id 39
		source 8
		target 10
		directed 0
	]
	edge
	[
		id 40
		source 9
		target 10
		directed 0
	]
	edge
	[
		id 41
		source 9
		target 11
		directed 0
	]
	edge
	[
		id 42
		source 10
		target 11
		directed 0
	]
	edge
	[
		id 43
		source 10
		target 12
		directed 0
	]
	edge
	[
		id 44
		source 11
		target 12
		directed 0
	]
	edge
	[
		id 45
		source 11
		target 13
		directed 0
	]
	edge
	[
		id 46
		source 12
		target 13
		directed 0
	]
	edge
	[
		id 47
		source 12
		target 14
		directed 0
	]
	edge
	[
		id 48
		source 13
		target 15
		directed 0
	]
	edge
	[
		id 49
		source 14
		target 15
		directed 0
	]
	edge
	[
		id 50
		source 14
		target 16
		directed 0
	]
	edge
	[
		id 51
		source 15
		target 16
		directed 0
	]
	edge
	[
		id 52
		source 15
		target 17
		directed 0
	]
	edge
	[
		id 53
		source 16
		target 17
		directed 0
	]
	edge
	[
		id 54
		source 16
		target 18
		directed 0
	]
	edge
	[
		id 55
		source 17
		target 19
		directed 0
	]
	edge
	[
		id 56
		source 18
		target 19
		directed 0
	]
]