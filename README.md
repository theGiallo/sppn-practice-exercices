# Social and Peer-to-Peer Networks practice exercices

## 00 - Generation of graph with Erdős–Rényi and Watts-Strogatz models
A creator of undirected graphs that uses the Erdős–Rényi model or the Watts and Strogatz model.

It generates a .gml file readable by gephi with nodes disposed in circle.

References:

[Erdős–Rényi model](https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93R%C3%A9nyi_model)

[Watts-Strogatz model](https://en.wikipedia.org/wiki/Watts_and_Strogatz_Model)

## Compile Instructions
You need a version of gcc that supports c++11. Simply run make_all.sh from it's directory.
If you want to build only an exercice run make in its directory.

## Run Instructions
Each exercise program prints its own instructions.
