#include "BitBucketCrawler.h"
#include <thread>

int main (int argc, char**argv)
{
	/*
		https://bitbucket.org/api/1.0/repositories/theGiallo/sdm/followers
		https://bitbucket.org/api/1.0/users/theGiallo/followers
		https://bitbucket.org/api/1.0/users/theGiallo/
	*/

	// init database
	// std::shared_ptr<BBC::BBUser> first_user(new BBC::BBUser());
	// first_user->username = "theGiallo";
	// BBC::new_users.push_back(first_user);

	BBC::crawler_run = false;
	// crawler
	std::thread crawler_thread(BBC::crawler);

	// UI
	std::thread UI_thread(BBC::UI);

	// crawler end
	crawler_thread.join();

	// UI end
	UI_thread.join();

	return 0;
}