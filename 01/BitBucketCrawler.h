#ifndef _BITBUCKET_CRAWLER_H_
#define _BITBUCKET_CRAWLER_H_

#include <string>
#include <map>
#include <vector>
#include <deque>
#include <list>
#include <memory>
#include <mutex>


namespace BBC
{

// trim function

const std::string whiteSpaces( " \f\n\r\t\v" );
std::string& trimRight( std::string& str, const std::string& trimChars = whiteSpaces );
std::string& trimLeft( std::string& str, const std::string& trimChars = whiteSpaces );
std::string& trim( std::string& str, const std::string& trimChars = whiteSpaces );


// REST

enum AuthType
{
	NONE  = 0x0,
	BASIC = 0x1,
	ASK   = 0x2
};

typedef struct rest_res_st
{
		bool failed;
		int code;
		std::map<std::string, std::string> header;
		std::string body;

		rest_res_st()
		{
			reset();
		}
		void reset()
		{
			failed = true;
			code = -1;
			header.clear();
			body = "";
		}
} RestRes;

size_t writeCallback(void *data, size_t size, size_t nmemb,void *userdata);
size_t headerCallback(void *data, size_t size, size_t nmemb,void *userdata);
RestRes& restQuery(RestRes& result, const std::string& url, AuthType auth_type = NONE, const std::string& user="", const std::string& pass="");


// BITBUCKET CRAWLER

class BBUser;
class BBRepository;

class BBRepoKey
{
public:
	std::string slug;
	std::string owner;

	BBRepoKey (std::string slug="", std::string owner="")
	: slug(slug), owner(owner)
	{}

	bool operator< (const BBRepoKey& k) const
	{
		return owner+"/"+slug < k.owner+"/"+k.slug;
	}

	template <typename Writer>
	void serialize(Writer& writer) const;
};

class BBUser
{
public:
	bool processed = false;
	std::string username;
	std::vector<std::shared_ptr<BBRepository>> repos_contributes;
	std::list<std::shared_ptr<BBRepository>> repos_follows;
	std::list<std::shared_ptr<BBUser>> users_follows;

	template <typename Writer>
	void serialize(Writer& writer) const;
};

class BBRepository
{
public:
	BBRepoKey key;
	std::map<std::string, std::shared_ptr<BBUser>> contributors;
};

extern std::map<std::string, std::shared_ptr<BBUser>> users;
extern std::map<BBRepoKey, std::shared_ptr<BBRepository>> repositories;
extern std::multimap<std::string, std::shared_ptr<BBRepository>> repos_by_slug;
extern std::multimap<std::string, std::shared_ptr<BBRepository>> repos_by_owner;

extern std::deque<std::shared_ptr<BBUser>> new_users;
extern std::deque<std::shared_ptr<BBRepository>> new_repositories;

extern std::mutex data_mutex;
extern bool crawler_run;
extern bool crawler_paused;

bool processRestUser(const RestRes& result);
bool processRestUserFollowers(const RestRes& result, std::shared_ptr<BBUser> user);
bool processRestRepoFollowers(const RestRes& result, std::shared_ptr<BBRepository> repo);

void writeData(const std::string& file_path);
bool loadData(const std::string& file_path);
void clearData();

// thread functions
void crawler();
void UI();


// GRAPHS

extern std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>> graph_users_following;
extern std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>> graph_users_following_repo_owner;
extern std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>> graph_users_following_repo_contributes;

void buildGraphUsersFollowing();
void buildGraphUsersFollowingRepoOwner();
void buildGraphUsersFollowingRepoContributes();

void clearGraphUsersFollowing();
void clearGraphUsersFollowingRepoOwner();
void clearGraphUsersFollowingRepoContributes();

bool writeGraphUsersFollowing(const std::string& file_path);
bool writeGraphUsersFollowingRepoOwner(const std::string& file_path);
bool writeGraphUsersFollowingRepoContributes(const std::string& file_path);

bool writeGraphUsers(const std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>>& graph, const std::string& file_path);

} // namespace BBC

#endif // _BITBUCKET_CRAWLER_H_