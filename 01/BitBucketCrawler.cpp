#include "BitBucketCrawler.h"

#include <curl/curl.h>
#include <rapidjson/document.h>
#include "rapidjson/filestream.h"	// wrapper of C stream for prettywriter as output
#include "rapidjson/writer.h"

#include <cstddef>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>

#include <thread>

#define SWITCH_COMPLETE
#include <generic_switch.h>

#include <unistd.h>

using namespace BBC;


std::map<std::string, std::shared_ptr<BBUser>> BBC::users;
std::map<BBRepoKey, std::shared_ptr<BBRepository>> BBC::repositories;
std::multimap<std::string, std::shared_ptr<BBRepository>> BBC::repos_by_slug;
std::multimap<std::string, std::shared_ptr<BBRepository>> BBC::repos_by_owner;

std::deque<std::shared_ptr<BBUser>> BBC::new_users;
std::deque<std::shared_ptr<BBRepository>> BBC::new_repositories;

std::mutex BBC::data_mutex;
bool BBC::crawler_run = false;
bool BBC::crawler_paused = true;


// trim function

std::string& BBC::trimRight( std::string& str, const std::string& trimChars)
{
   std::string::size_type pos = str.find_last_not_of( trimChars );
   str.erase( pos + 1 );

   return str;
}
std::string& BBC::trimLeft( std::string& str, const std::string& trimChars)
{
   std::string::size_type pos = str.find_first_not_of( trimChars );
   str.erase( 0, pos );
   return str;
}
std::string& BBC::trim( std::string& str, const std::string& trimChars)
{
   return trimRight( trimLeft( str, trimChars ), trimChars );
}

size_t BBC::writeCallback(void *data, size_t size, size_t nmemb,void *userdata)
{
	RestRes *result;
	result = reinterpret_cast<RestRes*>(userdata);
	std::string body(reinterpret_cast<char*>(data), size*nmemb);
	// std::cout<<"Body:"<<std::endl<<body<<std::endl<<"/Body"<<std::endl;
	result->body += body;
	return (size*nmemb);
}
size_t BBC::headerCallback(void *data, size_t size, size_t nmemb,void *userdata)
{
	RestRes *result;
	result = reinterpret_cast<RestRes*>(userdata);
	std::string header(reinterpret_cast<char*>(data), size*nmemb);
	// std::cout<<"Header:"<<std::endl<<header<<std::endl<<"/Header"<<std::endl;
	size_t separator_pos = header.find_first_of(":");
	if (separator_pos != std::string::npos)
	{
		std::string key = header.substr(0,separator_pos);
		trim(key);
		std::string value = header.substr(separator_pos+1);
		trim(value);

		result->header[key] = value;
	} else
	if (header.length()!=0)
	{
		result->header[header] = "present";
	}
	return (size*nmemb);
}

RestRes& BBC::restQuery(RestRes &result, const std::string& url, AuthType auth_type, const std::string& user, const std::string& pass)
{
	CURL *curl;
	CURLcode res;
	curl = curl_easy_init();
	if (!curl)
	{
		std::cerr<<"error initializing curl"<<std::endl;
	}

	switch (auth_type)
	{
	case ASK:
	{
		std::string user, pass, answer, y="y", n="n";
		bool not_correct = false;
		do
		{
			std::cout<<"Do you want to autenticate? [y/n] ";
			std::cin>>answer;
			if (answer==y)
			{
				std::cout<<"Username: ";
				std::cin>>user;
				std::cout<<"Password: ";
				std::cin>>pass;

				curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_easy_setopt(curl, CURLOPT_USERPWD,(user+":"+pass).c_str());
			} else
			// if (answer.compare("n")==0)
			if (answer != n)
			{
				not_correct = true;
			}
		} while (not_correct);
		break;
	}
	case BASIC:
		curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_easy_setopt(curl, CURLOPT_USERPWD,(user+":"+pass).c_str());
		break;
	case NONE:
		break;
	default:
		break;
	}
	curl_easy_setopt(curl,CURLOPT_USERAGENT, "test");
	curl_easy_setopt(curl,CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION, writeCallback);
	curl_easy_setopt(curl,CURLOPT_WRITEDATA, &result);
	curl_easy_setopt(curl,CURLOPT_HEADERFUNCTION, headerCallback);
	curl_easy_setopt(curl,CURLOPT_HEADERDATA, &result);

	res = curl_easy_perform(curl);
	if (res!=0)
	{
		result.failed = true;
	} else
	{
		result.failed = false;
		long http_code = 0;
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
		result.code = static_cast<int>(http_code);
	}

	curl_easy_cleanup(curl);

	return result;
}

bool BBC::processRestUser(const RestRes& result)
{
	std::string username;
	const char* str = result.body.c_str();
	rapidjson::Document document;
	if (document.Parse<0>(str).HasParseError())
	{
		std::cerr<<"Error parsing response"<<std::endl;
		return false;
	}

	// std::cout<<"document is object? "<<document.IsObject()<<std::endl;

	std::shared_ptr<BBUser> user(new BBUser());

	if (document.HasMember("user") && document["user"].IsObject())
	{
		// std::cout<<"username: "<<document["user"]["username"].GetString()<<std::endl;
		user->username = document["user"]["username"].GetString();
		std::map<std::string, std::shared_ptr<BBUser>>::iterator it;
		// users_mutex.lock();
		if ((it=users.find(user->username))==users.end())
		{
			users[user->username] = user;
		} else
		{
			user = it->second;
		}
		user->processed = true;
		// users_mutex.unlock();
	}

	if (document.HasMember("repositories") && document["repositories"].IsArray())
	{
		const rapidjson::Value& rr = document["repositories"];
		user->repos_contributes = std::vector<std::shared_ptr<BBRepository>>(rr.Size());
		for (rapidjson::SizeType i = 0; i < rr.Size(); i++)
		{
			if (rr[i].IsObject())
			{
				std::shared_ptr<BBRepository> repo(new BBRepository());
				if (rr[i].HasMember("slug"))
				{
					// std::cout<<"repository"<<std::endl<<"  slug: "<<
					// rr[i]["slug"].GetString()<<std::endl;
					repo->key.slug = rr[i]["slug"].GetString();
				}
				if (rr[i].HasMember("owner"))
				{
					// std::cout<<"  owner: "<<rr[i]["owner"].GetString()<<std::endl;
					repo->key.owner = rr[i]["owner"].GetString();
				}

				// repositories_mutex.lock();
				if (repositories.find(repo->key)==repositories.end())
				{
					repositories[repo->key] = repo;
					repos_by_owner.insert(std::pair<std::string,std::shared_ptr<BBRepository>>(repo->key.owner,repo));
					repos_by_slug.insert(std::pair<std::string,std::shared_ptr<BBRepository>>(repo->key.slug,repo));

					new_repositories.push_back(repo);
				}
				user->repos_contributes[i] = repo;
				if (user->username != repo->key.owner && users.find(repo->key.owner)==users.end())
				{
					std::shared_ptr<BBUser> owner(new BBUser());
					owner->username = repo->key.owner;
					users[owner->username] = owner;

					new_users.push_back(owner);
				}
				// repositories_mutex.lock();
			} else
			{
				return false;
			}
		}
	} else
	{
		return false;
	}

	return true;
}

bool BBC::processRestUserFollowers(const RestRes& result, std::shared_ptr<BBUser> user)
{
	const char* str = result.body.c_str();
	rapidjson::Document document;
	if (document.Parse<0>(str).HasParseError())
	{
		std::cerr<<"Error parsing response"<<std::endl;
		return false;
	}

	// std::cout<<"document is object? "<<document.IsObject()<<std::endl;

	if (document.HasMember("followers") && document["followers"].IsArray())
	{
		// std::cout<<"User "<<user->username<<" has "<<document["followers"].Size()<<(document["followers"].Size()==1?" follower":" followers")<<std::endl;

		std::shared_ptr<BBUser> follower;
		const rapidjson::Value& ff = document["followers"];
		for (rapidjson::SizeType i = 0; i < ff.Size(); i++)
		{
			if (ff[i].IsObject() && ff[i].HasMember("username"))
			{
				// users_mutex.lock();
				std::map<std::string, std::shared_ptr<BBUser>>::iterator uit;
				if ((uit=users.find(ff[i]["username"].GetString()))==users.end())
				{
					follower = std::shared_ptr<BBUser>(new BBUser());
					follower->username = ff[i]["username"].GetString();
					users[follower->username] = follower;
					new_users.push_back(follower);
				} else
				{
					follower = uit->second;
				}

				follower->users_follows.push_back(user);
				// users_mutex.unlock();
			} else
			{
				return false;
			}
		}
	} else
	{
		return false;
	}

	return true;
}

bool BBC::processRestRepoFollowers(const RestRes& result, std::shared_ptr<BBRepository> repo)
{
	const char* str = result.body.c_str();
	rapidjson::Document document;
	if (document.Parse<0>(str).HasParseError())
	{
		std::cerr<<"Error parsing response"<<std::endl;
		return false;
	}

	// std::cout<<"document is object? "<<document.IsObject()<<std::endl;

	if (document.HasMember("followers") && document["followers"].IsArray())
	{
		// std::cout<<"Repository "<<repo->key.owner<<"/"<<repo->key.slug<<" has "<<document["followers"].Size()<<(document["followers"].Size()==1?" follower":" followers")<<std::endl;

		std::shared_ptr<BBUser> follower;
		const rapidjson::Value& ff = document["followers"];
		for (rapidjson::SizeType i = 0; i < ff.Size(); i++)
		{
			if (ff[i].IsObject() && ff[i].HasMember("username"))
			{
				// users_mutex.lock();
				std::map<std::string, std::shared_ptr<BBUser>>::iterator uit;
				if ((uit=users.find(ff[i]["username"].GetString()))==users.end())
				{
					follower = std::shared_ptr<BBUser>(new BBUser());
					follower->username = ff[i]["username"].GetString();
					users[follower->username] = follower;
					new_users.push_back(follower);
				} else
				{
					follower = uit->second;
				}

				follower->repos_follows.push_back(repo);
				// users_mutex.unlock();
			} else
			{
				return false;
			}
		}
	} else
	{
		return false;
	}

	return true;
}


template <typename Writer>
void BBC::BBUser::serialize(Writer& writer) const
{
	writer.StartObject();

		writer.String("username");
		writer.String(username.c_str(), (rapidjson::SizeType)username.length());

		writer.String("processed");
		writer.Bool(processed);

		writer.String("repos_contributes");
		writer.StartArray();
		for (	std::vector<std::shared_ptr<BBRepository>>::const_iterator it = repos_contributes.begin();
				it!=repos_contributes.end();
				it++)
		{
			(*it)->key.serialize(writer);
		}
		writer.EndArray();

		writer.String("repos_follows");
		writer.StartArray();
		for (	std::list<std::shared_ptr<BBRepository>>::const_iterator it = repos_follows.begin();
				it!=repos_follows.end();
				it++)
		{
			(*it)->key.serialize(writer);
		}
		writer.EndArray();

		writer.String("users_follows");
		writer.StartArray();
		for (	std::list<std::shared_ptr<BBUser>>::const_iterator it = users_follows.begin();
				it!=users_follows.end();
				it++)
		{
			writer.StartObject();
			writer.String("username");
			writer.String((*it)->username.c_str(),(*it)->username.length());
			writer.EndObject();
		}
		writer.EndArray();

	writer.EndObject();
}


template <typename Writer>
void BBC::BBRepoKey::serialize(Writer& writer) const
{
	writer.StartObject();
		
		writer.String("slug");
		writer.String(slug.c_str(), slug.length());

		writer.String("owner");
		writer.String(owner.c_str(), owner.length());

	writer.EndObject();
}

void BBC::writeData(const std::string& file_path)
{
	data_mutex.lock();

	rapidjson::Document document;
	FILE* file_p;
	file_p = fopen(file_path.c_str(), "w");
	if (file_p==NULL)
	{
		std::cerr<<"Problem opening file!"<<std::endl;
		return;
	}
	rapidjson::FileStream file_stream(file_p);
	rapidjson::Writer<rapidjson::FileStream> writer(file_stream);

	writer.StartObject();

	writer.String("users");
	writer.StartArray();

		// put data in document
		for (	std::map<std::string, std::shared_ptr<BBUser>>::const_iterator it = users.begin();
				it!=users.end();
				it++)
		{
			it->second->serialize(writer);
		}

	writer.EndArray();

	writer.String("new_users");
	writer.StartArray();

		for (	std::deque<std::shared_ptr<BBUser>>::const_iterator it = new_users.begin();
				it!=new_users.end();
				it++)
		{
			writer.StartObject();
			writer.String("username");
			writer.String((*it)->username.c_str(), (*it)->username.length());
			writer.EndObject();
		}

	writer.EndArray();

	writer.String("new_repositories");
	writer.StartArray();

		for (	std::deque<std::shared_ptr<BBRepository>>::const_iterator it = new_repositories.begin();
				it!=new_repositories.end();
				it++)
		{
			(*it)->key.serialize(writer);
		}

	writer.EndArray();
	
	writer.EndObject();

	// document.Accept(writer);

	fclose(file_p);
	
	data_mutex.unlock();
}

bool BBC::loadData(const std::string& file_path)
{
	data_mutex.lock();

	int length;
	char* str;
	std::ifstream is(file_path);
	if (!is.is_open())
	{
		data_mutex.unlock();
		return false;
	}

	// get length of file:
    is.seekg (0, std::ios::end);
    length = is.tellg();
    is.clear();
    is.seekg (0, std::ios::beg);

    // allocate memory:
    str = new char [length+1];

    // read data as a block:
    is.read (str,length);
    is.close();
    str[length]=0;

	is.close();

	rapidjson::Document document;
	if (document.Parse<0>(str).HasParseError())
	{
		std::cerr<<"Error parsing response"<<std::endl;
		delete str;
		data_mutex.unlock();
		return false;
	}

	delete str;


	// read data

	if (!document.HasMember("users") || !document["users"].IsArray())
	{
		data_mutex.unlock();
		return false;
	}
	std::shared_ptr<BBUser> user, user_follows;
	std::shared_ptr<BBRepository> repo;

	const rapidjson::Value& uu = document["users"];
	for (rapidjson::SizeType i = 0; i < uu.Size(); i++)
	{
		// username
		if (!uu[i].IsObject() || !uu[i].HasMember("username") || !uu[i]["username"].IsString())
		{
			data_mutex.unlock();
			return false;
		}
		std::map<std::string, std::shared_ptr<BBUser>>::iterator uit;
		if ((uit=users.find(uu[i]["username"].GetString()))==users.end())
		{
			user = std::shared_ptr<BBUser>(new BBUser());
			user->username = uu[i]["username"].GetString();
			users[user->username] = user;
		} else
		{
			user = uit->second;
		}

		// processed
		if (!uu[i].HasMember("processed") || !uu[i]["processed"].IsBool())
		{
			data_mutex.unlock();
			return false;
		}
		user->processed = uu[i]["processed"].GetBool();

		// repos contributes
		if (!uu[i].HasMember("repos_contributes") ||
			!uu[i]["repos_contributes"].IsArray())
		{
			data_mutex.unlock();
			return false;
		}
		const rapidjson::Value& rr = uu[i]["repos_contributes"];
		for (rapidjson::SizeType j = 0; j < rr.Size(); j++)
		{
			if (!rr[j].IsObject())
			{
				data_mutex.unlock();
				return false;
			}

			BBRepoKey rk(rr[j]["slug"].GetString(), rr[j]["owner"].GetString());

			std::map<BBRepoKey, std::shared_ptr<BBRepository>>::iterator rit;
			if ((rit=repositories.find(rk))==repositories.end())
			{
				repo = std::shared_ptr<BBRepository>(new BBRepository());
				repo->key = rk;
				repositories[repo->key] = repo;
				repos_by_slug.insert(std::pair<std::string,std::shared_ptr<BBRepository>>(repo->key.slug, repo));
				repos_by_owner.insert(std::pair<std::string,std::shared_ptr<BBRepository>>(repo->key.owner, repo));
			} else
			{
				repo = rit->second;
			}
			repo->contributors[user->username];
			user->repos_contributes.push_back(repo);
		}

		// repos follows
		if (uu[i].HasMember("repos_follows") && uu[i]["repos_follows"].IsArray())
		{
			const rapidjson::Value& rr = document["users"][i]["repos_follows"];
			for (rapidjson::SizeType j = 0; j < rr.Size(); j++)
			{
				if (!rr[j].IsObject() ||
					!rr[j].HasMember("slug") || !rr[j]["slug"].IsString() ||
					!rr[j].HasMember("owner") || !rr[j]["owner"].IsString()
					)
				{
					data_mutex.unlock();
					return false;
				}

				BBRepoKey rk(rr[j]["slug"].GetString(), rr[j]["owner"].GetString());
				
				std::map<BBRepoKey, std::shared_ptr<BBRepository>>::iterator rit;
				if ((rit=repositories.find(rk))==repositories.end())
				{
					repo = std::shared_ptr<BBRepository>(new BBRepository());
					repo->key = rk;
					repositories[repo->key] = repo;
					repos_by_slug.insert(std::pair<std::string,std::shared_ptr<BBRepository>>(repo->key.slug, repo));
					repos_by_owner.insert(std::pair<std::string,std::shared_ptr<BBRepository>>(repo->key.owner, repo));
				} else
				{
					repo = rit->second;
				}
				user->repos_follows.push_back(repo);
				// repo->followers[user->username];
			}
		}
		// users follows
		const rapidjson::Value& uuf = uu[i]["users_follows"];
		for (rapidjson::SizeType j = 0; j < uuf.Size(); j++)
		{
			if (!uuf[j].IsObject() ||
				!uuf[j].HasMember("username") || !uuf[j]["username"].IsString())
			{
				data_mutex.unlock();
				return false;
			}

			std::map<std::string, std::shared_ptr<BBUser>>::iterator uit;
			if ((uit=users.find(uuf[j]["username"].GetString()))==users.end())
			{
				user_follows = std::shared_ptr<BBUser>(new BBUser());
				user_follows->username = uuf[j]["username"].GetString();
				users[user_follows->username] = user_follows;
			} else
			{
				user_follows = uit->second;
			}
			user->users_follows.push_back(user_follows);
		}
	}

	// new users
	if (!document.HasMember("new_users") || !document["new_users"].IsArray())
	{
		data_mutex.unlock();
		return false;
	}
	const rapidjson::Value& nuu = document["new_users"];
	for (rapidjson::SizeType j = 0; j < nuu.Size(); j++)
	{
		if (!nuu[j].IsObject() ||
			!nuu[j].HasMember("username") || !nuu[j]["username"].IsString())
		{
			data_mutex.unlock();
			return false;
		}

		std::map<std::string, std::shared_ptr<BBUser>>::iterator uit;
		if ((uit=users.find(nuu[j]["username"].GetString()))==users.end())
		{
			user = std::shared_ptr<BBUser>(new BBUser());
			user->username = nuu[j]["username"].GetString();
			users[user->username] = user;
		} else
		{
			user = uit->second;
		}

		new_users.push_back(user);
	}

	// new repos
	if (!document.HasMember("new_repositories") || !document["new_repositories"].IsArray())
	{
		data_mutex.unlock();
		return false;
	}
	const rapidjson::Value& nrr = document["new_repositories"];
	for (rapidjson::SizeType j = 0; j < nrr.Size(); j++)
	{
		if (!nrr[j].IsObject() ||
			!nrr[j].HasMember("slug") || !nrr[j]["slug"].IsString() ||
			!nrr[j].HasMember("owner") || !nrr[j]["owner"].IsString()
			)
		{
			data_mutex.unlock();
			return false;
		}

		BBRepoKey rk(nrr[j]["slug"].GetString(), nrr[j]["owner"].GetString());

		std::map<BBRepoKey, std::shared_ptr<BBRepository>>::iterator rit;
		if ((rit=repositories.find(rk))==repositories.end())
		{
			repo = std::shared_ptr<BBRepository>(new BBRepository());
			repo->key = rk;
			repositories[repo->key] = repo;
			repos_by_slug.insert(std::pair<std::string,std::shared_ptr<BBRepository>>(repo->key.slug, repo));
			repos_by_owner.insert(std::pair<std::string,std::shared_ptr<BBRepository>>(repo->key.owner, repo));
		} else
		{
			repo = rit->second;
		}

		new_repositories.push_back(repo);
	}

	data_mutex.unlock();
	return true;
}
void BBC::clearData()
{
	data_mutex.lock();

	users.clear();
	repositories.clear();
	repos_by_owner.clear();
	repos_by_slug.clear();
	new_users.clear();
	new_repositories.clear();

	data_mutex.unlock();
}

void BBC::crawler()
{
	while (!crawler_run){crawler_paused=true; std::this_thread::yield();}
	crawler_paused=false;

	FILE *fout;
	if ((fout=fopen("./nomeacaso","w+"))==0)
	{
		std::cerr<<"caccona"<<std::endl;
	}

	std::ofstream outs("./nomeacaso");

	std::shared_ptr<BBUser> user;
	// new_users_mutex.lock();
	while (!new_users.empty())
	{
		user = new_users.front();
		new_users.pop_front();
		// new_users_mutex.unlock();
		while (!crawler_run){crawler_paused=true; std::this_thread::yield();}
		crawler_paused=false;

		RestRes result;

		outs<<"Processing user"<<std::endl<<
		"  username: "<<user->username<<std::endl;

		data_mutex.lock();

		// query user
		while (true)
		{
			result.reset();
			restQuery(result, "https://bitbucket.org/api/1.0/users/"+user->username);
			if (result.failed)
			{
				std::cerr<<"Failed query: "<<"https://bitbucket.org/api/1.0/users/"+user->username<<" at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
				outs<<
				"failed "<<result.failed<<std::endl<<
				"code "<<result.code<<std::endl<<
				"body:"<<std::endl<<result.body<<std::endl;
				sleep(1);
				std::cerr<<"Retrying..."<<std::endl;
				continue;
			}
			

			if (!result.failed && result.code == 200)
			{
				if (!processRestUser(result))
				{
					std::cerr<<"Error parsing rest user response for "<<user->username<<std::endl;
					std::cerr<<
					"failed "<<result.failed<<std::endl<<
					"code "<<result.code<<std::endl<<
					"body:"<<std::endl<<result.body<<std::endl;
				}
			} else
			if (! (result.code%400 < 100))
			{
				if ((result.code%500 < 100))
				{
					sleep(1);
				}
				std::cerr<<"Retrying..."<<std::endl;
				continue;
			}
			break;
		}

		outs<<"Processing user's followers"<<std::endl<<
		"  username: "<<user->username<<std::endl;

		// query user's followers
		while (true)
		{
			result.reset();
			restQuery(result, "https://bitbucket.org/api/1.0/users/"+user->username+"/followers");
			if (result.failed)
			{
				std::cerr<<"Failed query: "<<"https://bitbucket.org/api/1.0/users/"+user->username+"/followers"<<" at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
				outs<<
				"failed "<<result.failed<<std::endl<<
				"code "<<result.code<<std::endl<<
				"body:"<<std::endl<<result.body<<std::endl;
				sleep(1);
				std::cerr<<"Retrying..."<<std::endl;
				continue;
			}

			if (!result.failed && result.code == 200)
			{
				if (!processRestUserFollowers(result,user))
				{
					std::cerr<<"Error parsing rest user's followers response for "<<user->username<<std::endl;
					std::cerr<<
					"failed "<<result.failed<<std::endl<<
					"code "<<result.code<<std::endl<<
					"body:"<<std::endl<<result.body<<std::endl;
				}
			} else
			if (! (result.code%400 < 100))
			{
				if ((result.code%500 < 100))
				{
					sleep(1);
				}
				std::cerr<<"Retrying..."<<std::endl;
				continue;
			}
			break;
		}

		data_mutex.unlock();

		std::shared_ptr<BBRepository> repo;
		// new_repos_mutex.lock();
		while(!new_repositories.empty())
		{
			data_mutex.lock();
			repo = new_repositories.front();
			new_repositories.pop_front();
			// new_repos_mutex.unlock();

			outs<<"Processing repository"<<std::endl<<
			"  slug: "<<repo->key.slug<<std::endl<<
			"  owner: "<<repo->key.owner<<std::endl;

			// query repo's followers
			while (true)
			{
				result.reset();
				restQuery(result, "https://bitbucket.org/api/1.0/repositories/"+repo->key.owner+"/"+repo->key.slug+"/followers");
				if (result.failed)
				{
					std::cerr<<"Failed query: "<<"https://bitbucket.org/api/1.0/repositories/"+repo->key.owner+"/"+repo->key.slug+"/followers"<<" at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
					outs<<
					"failed "<<result.failed<<std::endl<<
					"code "<<result.code<<std::endl<<
					"body:"<<std::endl<<result.body<<std::endl;
					sleep(1);

					std::cerr<<"Retrying..."<<std::endl;
					continue;
				}

				if (!result.failed && result.code == 200)
				{
					if (!processRestRepoFollowers(result, repo))
					{
						std::cerr<<"Error parsing rest repo's followers response for "<<repo->key.owner<<"/"<<repo->key.slug<<std::endl;
						std::cerr<<
						"failed "<<result.failed<<std::endl<<
						"code "<<result.code<<std::endl<<
						"body:"<<std::endl<<result.body<<std::endl;
					}
				} else
				if (! (result.code%400 < 100))
				{
					if ((result.code%500 < 100))
					{
						sleep(1);
					}
					if (result.body=="Repository currently not available.\n")
					{
						std::cout<<"Repository unavailable."<<std::endl<<
							"   owner: "<<repo->key.owner<<std::endl<<
							"    slug: "<<repo->key.slug<<std::endl;
						outs<<"Repository unavailable."<<std::endl<<
							"   owner: "<<repo->key.owner<<std::endl<<
							"    slug: "<<repo->key.slug<<std::endl;
						break;
					}
					std::cout<<
					"code "<<result.code<<std::endl<<
					"body:"<<std::endl<<result.body<<std::endl;
					std::cerr<<"Retrying..."<<std::endl;
					continue;
				}
				break;
			}
			data_mutex.unlock();
			// new_repos_mutex.lock();
		}
		// new_repos_mutex.unlock();

		// new_users_mutex.lock();
	}
	// new_users_mutex.unlock();

	std::cout<<"No more new users to process"<<std::endl;

	fclose(fout);
	outs.close();
}

void BBC::UI()
{
	std::string a_string;

	while (true)
	{
		std::getline (std::cin,a_string);

		SWITCH (a_string)
		{
		CASE ({"users count"})
			std::cout<<users.size()<<std::endl;
			break;
		CASE ({"start"})
			if (crawler_run)
			{
				std::cout<<"already running!"<<std::endl;
			}
			crawler_run = true;
			while(crawler_paused){sleep(1);std::this_thread::yield();}
			std::cout<<"running"<<std::endl;
			break;
		CASE ({"pause"})
			if (!crawler_run)
			{
				std::cout<<"not running!"<<std::endl;
			} else
			{
				crawler_run = false;
				while(!crawler_paused){std::this_thread::yield();}
				std::cout<<"paused"<<std::endl;
			}
			break;
		CASE ({"save as"})
			std::cout<<"where I have to write? ";
			std::getline (std::cin,a_string);
			std::cout<<"saving... "; std::cout.flush();
			writeData(a_string);
			std::cout<<"done."<<std::endl; std::cout.flush();
			break;
		CASE ({"load"})
			std::cout<<"where I have to load from? ";
			std::getline (std::cin,a_string);
			std::cout<<"loading... "; std::cout.flush();
			loadData(a_string);
			std::cout<<"done."<<std::endl; std::cout.flush();
			break;
		CASE ({"clear"})
			bool not_answered = true;
			while (not_answered)
			{
				std::cout<<"All data will be deleted. Do you want to proceed? [Yes, No] ";
				std::getline (std::cin,a_string);

				SWITCH (a_string)
				{
				CASE ({"Yes"})
					clearData();
					std::cout<<"All data have been deleted"<<std::endl;
					not_answered = false;
					break;
				CASE ({"No"})
					not_answered = false;
					break;
				DEFAULT
					std::cout<<"Please write exactly \"Yes\" or \"No\""<<std::endl;
					break;
				} SWITCHEND
			}
			break;
		CASE ({"save graph f"})
			std::cout<<"where I have to write? ";
			std::getline (std::cin,a_string);
			clearGraphUsersFollowing();
			std::cout<<"builging graph... "; std::cout.flush();
			buildGraphUsersFollowing();
			std::cout<<"done."<<std::endl;
			std::cout<<"writing graph... "; std::cout.flush();
			writeGraphUsersFollowing(a_string);
			std::cout<<"done."<<std::endl;
			clearGraphUsersFollowing();
			break;
		CASE ({"save graph fro"})
			std::cout<<"where I have to write? ";
			std::getline (std::cin,a_string);
			clearGraphUsersFollowingRepoOwner();
			std::cout<<"builging graph... "; std::cout.flush();
			buildGraphUsersFollowingRepoOwner();
			std::cout<<"done."<<std::endl;
			std::cout<<"writing graph... "; std::cout.flush();
			writeGraphUsersFollowingRepoOwner(a_string);
			std::cout<<"done."<<std::endl;
			clearGraphUsersFollowingRepoOwner();
			break;
		CASE ({"save graph frc"})
			std::cout<<"where I have to write? ";
			std::getline (std::cin,a_string);
			clearGraphUsersFollowingRepoContributes();
			std::cout<<"builging graph... "; std::cout.flush();
			buildGraphUsersFollowingRepoContributes();
			std::cout<<"done."<<std::endl;
			std::cout<<"writing graph... "; std::cout.flush();
			writeGraphUsersFollowingRepoContributes(a_string);
			std::cout<<"done."<<std::endl;
			clearGraphUsersFollowingRepoContributes();
			break;
		CASE ({"help"})
			std::cout<<"Commands:"<<std::endl<<std::endl<<
			"    start            starts the crawler"<<std::endl<<
			"    pause            pauses the crawler"<<std::endl<<
			"    save as	      saves the data in a JSON file"<<std::endl<<
			"    load             loads data from a JSON file"<<std::endl<<
			"    clear            clears all the data"<<std::endl<<
			"    users count      prints the number of users in the data"<<std::endl<<
			"    save graph f     generate and save the graph of users with following relationship"<<std::endl<<
			"    save graph fro   generate and save the graph of users with following repository user owns relationship"<<std::endl<<
			"    save graph frc   generate and save the graph of users with following repository user contributes to relationship"<<std::endl<<std::endl;
			break;
		DEFAULT
			std::cout<<"unrecognised request"<<std::endl;
		} SWITCHEND
	}
}


// GRAPHS

std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>> BBC::graph_users_following;
std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>> BBC::graph_users_following_repo_owner;
std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>> BBC::graph_users_following_repo_contributes;

void BBC::buildGraphUsersFollowing()
{
	data_mutex.lock();
	for (	std::map<std::string, std::shared_ptr<BBUser>>::const_iterator it = users.begin();
			it!=users.end();
			it++)
	{
		for (	std::list<std::shared_ptr<BBUser>>::const_iterator fit = it->second->users_follows.begin();
				fit!=it->second->users_follows.end();
				fit++)
		{
			graph_users_following.insert(std::pair<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>>(it->second, *fit));
		}
	}
	data_mutex.unlock();
}
void BBC::buildGraphUsersFollowingRepoOwner()
{
	data_mutex.lock();
	for (	std::map<std::string, std::shared_ptr<BBUser>>::const_iterator it = users.begin();
			it!=users.end();
			it++)
	{
		for (	std::list<std::shared_ptr<BBRepository>>::const_iterator rit = it->second->repos_follows.begin();
				rit!=it->second->repos_follows.end();
				rit++)
		{
			graph_users_following_repo_owner.insert(std::pair<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>>(it->second, users[(*rit)->key.owner]));
		}
	}
	data_mutex.unlock();
}
void BBC::buildGraphUsersFollowingRepoContributes()
{
	data_mutex.lock();
	for (	std::map<std::string, std::shared_ptr<BBUser>>::const_iterator it = users.begin();
			it!=users.end();
			it++)
	{
		for (	std::list<std::shared_ptr<BBRepository>>::const_iterator rit = it->second->repos_follows.begin();
				rit!=it->second->repos_follows.end();
				rit++)
		{
			for (	std::map<std::string, std::shared_ptr<BBUser>>::const_iterator cit = (*rit)->contributors.begin();
					cit!=(*rit)->contributors.end();
					cit++)
			{
				graph_users_following_repo_owner.insert(std::pair<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>>(it->second, cit->second));
			}
		}
	}
	data_mutex.unlock();
}

void BBC::clearGraphUsersFollowing()
{
	graph_users_following.clear();
}
void BBC::clearGraphUsersFollowingRepoOwner()
{
	graph_users_following_repo_owner.clear();
}
void BBC::clearGraphUsersFollowingRepoContributes()
{
	graph_users_following_repo_contributes.clear();
}

bool BBC::writeGraphUsersFollowing(const std::string& file_path)
{
	return writeGraphUsers(graph_users_following, file_path);
}
bool BBC::writeGraphUsersFollowingRepoOwner(const std::string& file_path)
{
	return writeGraphUsers(graph_users_following_repo_owner, file_path);
}
bool BBC::writeGraphUsersFollowingRepoContributes(const std::string& file_path)
{
	return writeGraphUsers(graph_users_following_repo_contributes, file_path);
}

bool BBC::writeGraphUsers(const std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>>& graph, const std::string& file_path)
{
	std::ofstream fout;
	fout.open(file_path);
	if (!fout.is_open())
	{
		return false;
	}

  	fout << "graph"<<std::endl<<"["<<std::endl;

	std::map<std::shared_ptr<BBUser>, int> users_ids;

	int id = 0;
	for (	std::map<std::string, std::shared_ptr<BBUser>>::const_iterator it = users.begin();
			it!=users.end();
			id++, it++)
	{
		fout <<"	node"<<std::endl
			 <<"	["<<std::endl
			 <<"		id "<<id<<std::endl
			 <<"		label \""<<it->second->username<<"\""<<std::endl
			 // <<"		graphics"<<std::endl
			 // <<"		["<<std::endl
			 // <<"			x "<<radius*cos(angle*node.first)<<std::endl
			 // <<"			y "<<radius*sin(angle*node.first)<<endl
			 // <<"			z 0.0"<<std::endl
			 // <<"			w 1.0"<<std::endl
			 // <<"			h 1.0"<<std::endl
			 // <<"			fill \"#999999\""<<std::endl
			 // <<"		]"<<std::endl
			 <<"	]"<<std::endl;

		users_ids[it->second] = id;
	}

	int eid = 0;
	for (	std::map<std::string, std::shared_ptr<BBUser>>::const_iterator it = users.begin();
			it!=users.end();
			it++)
	{
		std::pair <std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>>::const_iterator, std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>>::const_iterator> es;
	    es = graph.equal_range(it->second);
	    for (std::multimap<std::shared_ptr<BBUser>,std::shared_ptr<BBUser>>::const_iterator eit=es.first; eit!=es.second; eit++, eid++)
		{
			if (users_ids[it->second] != users_ids[eit->second] )
			{
				fout <<"	edge"<<std::endl
					 <<"	["<<std::endl
					 <<"		id "<<eid<<std::endl
					 <<"		source "<<users_ids[it->second]<<std::endl
					 <<"		target "<<users_ids[eit->second]<<std::endl
					 <<"		directed 1"<<std::endl
					 <<"	]"<<std::endl;
			}
		}
	}

  	fout << "]";

  	fout.close();
}
