#include "generic_switch.h"
#include <vector>
#include <iostream>
#include <initializer_list>

class Comparable
{
public:
	Comparable(int v=0):v(v){};
	Comparable(const std::initializer_list<int>& vl):v(*vl.begin()){};
	int v;

	bool operator ==(Comparable c) const
	{
		return v==c.v;
	}
	bool operator ==(const std::initializer_list<int>& vl) const
	{
		return v==*vl.begin();
	}
	const Comparable operator =(const std::initializer_list<int>& vl)
	{
		v=*vl.begin();
		return *this;
	}
};

// template<int>
// std::initializer_list<int>::operator Comparable() const
// {
// 	return Comparable(*begin());
// }

int main ()
{
	Comparable c0(0), c1(1), c2(2), c3({3});

	std::cout<<c0.v<<"=="<<c1.v<<" "<<(equalsOR(c0,c1)?"true":"false")<<std::endl;
	std::cout<<c0.v<<"=="<<c1.v<<" || "<<c0.v<<"=="<<c2.v<<" "<<(equalsOR(c0,c1,c2)?"true":"false")<<std::endl;
	std::cout<<c0.v<<"=="<<c1.v<<" || "<<c0.v<<"=="<<c1.v<<" "<<(equalsOR(c0,c1,c1)?"true":"false")<<std::endl;
	std::cout<<c0.v<<"=="<<c1.v<<" || "<<c0.v<<"=="<<c0.v<<" "<<(equalsOR(c0,c1,c0)?"true":"false")<<std::endl;
	std::cout<<c0.v<<"=="<<c0.v<<" || "<<c0.v<<"=="<<c1.v<<" "<<(equalsOR(c0,c0,c1)?"true":"false")<<std::endl;


	std::cout<<c0.v<<"=="<<1<<" "<<(equalsOR(c0, {1})?"true":"false")<<std::endl;

#if defined(SWITCH_BREAK) && defined(SWITCH_NOBRACKETS)
	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
		CASE(Comparable(2))
		CASE(Comparable(0))
		CASE(Comparable(4))
			std::cout<<"case 2,0,4 (separated, with fallthrough)"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
			break;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
			break;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
		DEFAULT
			std::cout<<"default"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
			break;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		DEFAULT
			std::cout<<"default"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;
#endif

#if !defined(SWITCH_BREAK) && defined(SWITCH_NOBRACKETS)
	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
		CASE(Comparable(2))
		CASE(Comparable(0))
		CASE(Comparable(4))
			std::cout<<"case 2,0,4 (separated, with fallthrough)"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;
	
	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
		DEFAULT
			std::cout<<"default"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		DEFAULT
			std::cout<<"default"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;
#endif
#if defined(SWITCH_BREAK) && !defined(SWITCH_NOBRACKETS)
	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
		CASE(Comparable(2)){}
		CASE(Comparable(0));
		CASE(Comparable(4))
		{
			std::cout<<"case 2,0,4 (separated, with fallthrough)"<<std::endl;
		}
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;
	
	SWITCH(c0)
	{
		CASE({1})
		{
			std::cout<<"case 1"<<std::endl;
		}
		CASE({0})
		{
			std::cout<<"case 0"<<std::endl;
		}
		CASE({5})
		{
			std::cout<<"case 5"<<std::endl;
		}
		CASE({0})
		{
			std::cout<<"case 0"<<std::endl;
			break;
		}
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
		{
			std::cout<<"case 2,3,4"<<std::endl;
		}
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
		{
			std::cout<<"case 2,0,4"<<std::endl;
		}
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
		{
			std::cout<<"case 1"<<std::endl;
		}
		CASE({3})
		{
			std::cout<<"case 3"<<std::endl;
		}
		CASE({5})
		{
			std::cout<<"case 5"<<std::endl;
		}
		CASE({3})
		{
			std::cout<<"case 3"<<std::endl;
			break;
		}
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
		{
			std::cout<<"case 2,3,4"<<std::endl;
		}
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
		{
			std::cout<<"case 2,0,4"<<std::endl;
		}
		DEFAULT
		{
			std::cout<<"default"<<std::endl;
		}
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
		{
			std::cout<<"case 1"<<std::endl;
		}
		CASE({3})
		{
			std::cout<<"case 3"<<std::endl;
		}
		CASE({5})
		{
			std::cout<<"case 5"<<std::endl;
		}
		CASE({3})
		{
			std::cout<<"case 3"<<std::endl;
			break;
		}
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
		{
			std::cout<<"case 2,3,4"<<std::endl;
		}
		DEFAULT
		{
			std::cout<<"default"<<std::endl;
		}
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;
#endif

#if !defined(SWITCH_BREAK) && !defined(SWITCH_NOBRACKETS)
	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
		CASE(Comparable(2)){}
		CASE(Comparable(0));
		CASE(Comparable(4))
		{
			std::cout<<"case 2,0,4 (separated, with fallthrough)"<<std::endl;
		}
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;
	
	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({0})
			std::cout<<"case 0"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		CASE(Comparable(2),
			 Comparable(0),
			 Comparable(4))
			std::cout<<"case 2,0,4"<<std::endl;
		DEFAULT
			std::cout<<"default"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;

	SWITCH(c0)
	{
		CASE({1})
			std::cout<<"case 1"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE({5})
			std::cout<<"case 5"<<std::endl;
		CASE({3})
			std::cout<<"case 3"<<std::endl;
		CASE(Comparable(2),
			 Comparable(3),
			 Comparable(4))
			std::cout<<"case 2,3,4"<<std::endl;
		DEFAULT
			std::cout<<"default"<<std::endl;
	}HCTIWS

	std::cout<<"switch ended"<<std::endl<<std::endl;
#endif
	
	return 0;
}


/*
//max
template<typename T,typename ...A>
static DFORCEINLINE T max(T x,A... a){
return max(x,max(a...));
}
template<class T>
static DFORCEINLINE T max(T x,T y){
return x>y?x:y;
}*/