#!/bin/bash

echo -e "\n################################################################################\ntest_switch_complete"
./test_switch_complete;

echo -e "\n################################################################################\ntest_switch_break"
./test_switch_break;

echo -e "\n################################################################################\ntest_switch_fallthrough"
./test_switch_fallthrough;

echo -e "\n################################################################################\ntest_switch_nobrackets"
./test_switch_nobrackets;

echo -e "\n################################################################################\ntest_switch_break_nobrackets"
./test_switch_break_nobrackets;

echo -e "\n################################################################################\ntest_switch_nobrackets_fallthrough"
./test_switch_nobrackets_fallthrough;

echo -e "\n################################################################################\ntest_switch_break_fallthrough"
./test_switch_break_fallthrough;

echo -e "\n################################################################################\ntest_switch_break_nobrackets_fallthrough"
./test_switch_break_nobrackets_fallthrough;